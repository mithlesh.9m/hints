# Basic Data Structures: Min Stack

<!-- Hint 1 -->
<details>
  <summary><b>Hint 1:</b>&nbsp;Basis of the problem</summary><br>

A stack is a container of objects that are inserted and removed according to the last-in-first-out (LIFO) principle. For the most basic stack implementation, only two operations are allowed:<br>
&emsp;\- push the element into the stack<br>
&emsp;\- pop the element out of the stack<br>

A stack is also called a limited-access data structure, because the elements can be added and removed from the stack only at the top. 

<img src="images/basic-data-structures/min-stack/min-stack-1.png"  width="800"><br>

Here is a basic stack implementation using a linked list as the value container:<br>
```ruby
# node class
class Node
  attr_accessor :value, :next_node
  def initialize value
    @value = value
  end
end

class Stack
  # insert an element into the stack
  def push(number)
    new_node = Node.new(number)
    if @top.nil?
      @top = new_node
    else
      new_node.next_node = @top
      @top = new_node
    end
  end
  # removes an element from the stack, then returns the removed node
  def pop
    return if @top.nil?
    
    removed_node = @top
    @top = @top.next_node
    removed_node
  end

  def empty?
    @top.nil?
  end
end
```

In the implementation above, the push and pop operations are handled without any loop or sequential process. For each case, the time complexity is **O(1)**.

--- 

<br>

In this challenge, we will implement another feature to get the minimum value with a constant time complexity -> O(1).

We will follow a three-step development approach to arrive at the solution. For more information, see [Make It Work. Make It Right. Make It Fast](https://www.weeklydevtips.com/episodes/006)

Let's start with a brute-force approach to make it work, which has a linear time complexity -> **`O(n)`**

A brute-force solution can be implemented by using an iterator that visits all of the stack elements sequentially, then finding the minimum value by comparing them during the iteration.

```ruby
# node class implementation omitted

class Stack
  # constructor and the other functions omitted

  # finds the minimum value with a brute-force search then returns it
  def min
    return if @top.nil?

    # start at the top element
    current = @top
    min_value = current.value

    until current.nil?
      if current.value < min_value
        min_value = current.value
      end
      # iterate through next_node of the current element
      current = current.next_node
    end
    min_value
  end
end
```

If we have **n** elements, the "min" function will process the loop **n** times. Therefore, it has linear time complexity, **O(n)**. It is not horrible, but we can optimize it so that it has constant time complexity (**O(1)**). [Here is](images/basic-data-structures/min-stack/min-stack-2.png) a diagram comparing the algorithm complexities.

---

<br>

Okay, we made it work. Now, let's think through how we can do better with a constant time complexity -> **`O(1)`**

Let's use an integer variable to keep track of the minimum value.

It works for the push operation.<br>
At each push operation, we can update the minimum value with a simple comparison:<br>
<img src="images/basic-data-structures/min-stack/min-stack-3.png"  width="800">

However, this may cause some critical issues for the pop operation.<br>
If the top element is the minimum when we remove it, the minimum value needs to be updated. How can we get the previous minimum element?

<img src="images/basic-data-structures/min-stack/min-stack-4.png"  width="800">

---

<br>

Here, to get the previous minimum element, we need to keep track of each minimum element we push.<br>
There are a couple of ways to do this; we will focus on an approach that is more effective.

We will use two stacks as instance variables of the MinStack class. One will contain all the elements we push; the other will contain only the minimum elements.<br>

<img src="images/basic-data-structures/min-stack/min-stack-5.png"  width="800"><br>

<b>`Hint:`&nbsp;Use "main_stack", an instance of the Stack class, to keep track of each element.<br>
<b>`Hint:`&nbsp;Use "min_stack", an instance of the Stack class, to keep track of the minimum elements. The top instance of "min_stack" should be the minimum element.<br>
<b>`Hint:`&nbsp;At the push operation, consider whether the added element is smaller than the minimum element.<br>
<b>`Hint:`&nbsp;At the pop operation, consider whether the removed element is the minimum element.<br>

</details>
<!-- Hint 1 -->

---

<!-- Hint 2 -->
<details>
  <summary><b>Hint 2:</b>&nbsp;How to insert an element</summary><br>

The structure of the MinStack class:
```ruby
class Node
  # node implementation omitted
end

class Stack
  # stack implementation omitted
end

class MinStack
  def initialize
    @main_stack = Stack.new
    @min_stack = Stack.new
  end

  # inserts an element
  def push(number)
  end

  # removes the top element
  def pop(number)
  end

  # returns the minimum value
  def min
  end
end
```

<img src="images/basic-data-structures/min-stack/min-stack-6.png"  width="800"><br>

<b>`Hint:`&nbsp;Push the new element into **main_stack**.<br>
<b>`Hint:`&nbsp;If **min_stack** is empty, push the number into **min_stack** as well.<br>
<b>`Hint:`&nbsp;Otherwise, compare the newest element with the current minimum. If it is smaller than the current minimum, push it into **min_stack** as well.<br>
<b>`Hint:`&nbsp;The "min" method is only responsible for returning the top element from min_stack. Keep in mind that it can be empty.<br>

<b>`Note:`</b>&nbsp;A MinStack class has two stacks. Generally, the **has-a** association between two classes should be handled by a technique called **composition** as done above. For more information, check out [this link](https://javapapers.com/oops/association-aggregation-composition-abstraction-generalization-realization-dependency/). It is an important topic that is frequently asked in interviews.<br>

</details>
<!-- Hint 2 -->

---

<!-- Hint 3 -->
<details>
  <summary><b>Hint 3:</b>&nbsp;How to remove an element</summary><br>

The push operation can be implemented like this:
```ruby
# node implementation omitted
class Node end;
# stack implementation omitted
class Stack end;

class MinStack
  # constructor and other methods omitted

  def push(number)
    # push the number into the main stack
    @main_stack.push(number)
    # push the number into the min stack if the conditions are met
    @min_stack.push(number) if @min_stack.empty? || @min_stack.top.value >= number
  end
end
```

<img src="images/basic-data-structures/min-stack/min-stack-7.png"  width="800"><br>

<b>`Hint:`&nbsp;Remove the top element from **main_stack**. It will return the removed element; keep it.<br>
<b>`Hint:`&nbsp;If the removed element is equal to the top of **min_stack**, remove the top element from **min_stack** as well. Otherwise, do nothing.<br>
  
</details>
<!-- Hint 3 -->

---

<!-- Solution -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution</summary><br>

```ruby
class Node
  # node implementation omitted
end

class Stack
  # stack implementation omitted
end

class MinStack
  def initialize
    # initialize the stacks
    @main_stack = Stack.new
    @min_stack = Stack.new
  end

  def push(number)
    # push the number into the main stack
    @main_stack.push(number)
    # push the number into min_stack if the conditions are met
    @min_stack.push(number) if @min_stack.empty? || @min_stack.top.value >= number
  end

  def pop
    # remove the element from the top of main_stack
    removed_node = @main_stack.pop
    # if the conditions are met, remove it from the top of min_stack as well
    @min_stack.pop if removed_node && @min_stack.top.value == removed_node.value
    # return the removed node
    removed_node
  end

  def min
    # if there are no elements in min_stack, simply return nil
    return nil if @min_stack.empty?
    # return the value of the top instance of min_stack
    @min_stack.top.value
  end
end
```
  
</details>
<!-- Solution -->

