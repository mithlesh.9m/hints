# Sorting Algorithms: Insertion Sort

<!-- Hint -->
<details>
  <summary><b>Hint:</b>&nbsp;Basis of the problem</summary><br>

Insertion sort is a sorting algorithm that places a misplaced element in its correct place at each iteration.

In the previous challenges, we placed an element from only the end of the array in its correct place.

The logic of the insertion algorithm is simple:<br>
&emsp;\- First, we need to open up the correct space for the misplaced element. To achieve this, we shift each larger element to its next index.<br>
&emsp;\- Now,the correct place is available. We can simply place the value into that index.

The algorithm flow is shown step by step in the picture below:<br>
<img src="images/sorting-algorithms/insertion-sort-itself/insertion-2-1.png"  width="800"><br>

The algorithm above can be implemented as:<br>

```ruby
def insertion_sort(array)
  value_to_insert = array[-1]
  j = array.length - 2
  # shift greater elements while they exist
  while j >= 0 && value_to_insert < array[j]
    array[j + 1] = array[j]
    j = j - 1
  end
  # after the loop, all the greater elements are shifted
  # then the correct place (j + 1) is available
  array[j + 1] = number
end
```

---

<br>

In this challenge, we will use the same insertion method to sort an entire array.

It is critical to note that the above implementation works for a given element only when all of its prior element(s) are already sorted. To prevent errors, ensure that the elements that are placed before the one being processed are already sorted.

The complete algorithm flow is shown in the animation below:<br>
<img src="images/sorting-algorithms/insertion-sort-itself/insertion-2-2.gif"  width="500">

<b>`Hint:`&nbsp;Apply the same insertion method for each element, starting from the second element to the last element of the array.<br>
<b>`Hint:`&nbsp;At the end of each iteration, print the actual array.<br>

<b>`Note:`</b>&nbsp;You can use **puts array.join(' ')** to print the array.<br>
  
</details>
<!-- Hint 1 -->

---

<!-- Solution -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution</summary><br>
  
```ruby
def sort_itself(array)
  for i in 1...array.length
    value_to_insert = array[i]
    j = i - 1
    while j >= 0 && value_to_insert < array[j]
      array[j + 1] = array[j]
      j = j - 1
    end
    array[j + 1] = value_to_insert
    puts array.join(' ')
  end
end
```

</details>
<!-- Hint -->
